## Ogilvy - Email Automation

A sample application that reads a user's gmail feed for unread emails. The
automation is triggered based on an email containing a given string.

To get credentials for the application, follow these steps:

1. Navigate to the Google API console to register the a new app
2. Create a new set of credentials (NB OAuth Client ID not Service Account Key and then choose "Web Application" from the selection)
3. Include https://developers.google.com/oauthplayground as a valid redirect URI
4. Save the client ID (web app) and Client Secret
5. Go to Oauth2 playground
6. In Settings (gear icon), set
    * Oauth flow: server
    * Access type: offline
    * Use your own OAuth credentials: TICK
    * Client Id and Client Secret: from step 4
7. Click Step 1 and choose the Gmail Feed API
8. Click Authorize APIs. You will be prompted to choose your Google account and confirm access
9. Click Step 2 and "Exchange Authorization code for tokens"
10. Copy the returned Refresh Token
11. Use the Client ID, Client Secret, and Refresh Token in the source code.