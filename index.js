const axios = require('axios');
var qs = require('qs');
var nodemailer = require('nodemailer');

// *************** Configurations ***************
const clientID = "671102291573-n205rlbu1tuj809fhn2a4oq621puu6fr.apps.googleusercontent.com";    // Client ID
const clientSecret = "J_s9s0OjpHoEjsykMA8MWvNR";                                                // Client Secret
const refreshToken = "1/oh3V8pI37VsB2Hffxuz_cjmg-gUktlRBnwDDEcqtk0o"                            // Refresh Token for the user's Oauth
const doubleClickProfileIDs = ["4792321", "4789886", "4744132"];                                // Profile ID of DoubleCLick Automation Account
const threadLabels = ['UNREAD', 'Label_11']                                                     // Thread Labels to search for                                                                                               // by the Axios Library
// ************* End Configurations *************

// **************** Constants *******************
const automatedEmailSender = "passage.developer@gmail.com";       // Sender's email address for followups
const automatedEmailPass = "AV1UONktc91TFv3Ayonz4F";              // Password for automated email address
const emailDateRegex = new RegExp(/\d{1,2}\/\d{1,2}\/\d{4}/);     // Regex Expression for date checking in the email
const followUpEmailSubject = "Automation Completed From User: @@" // Format of the subject to be sent as confirmation to automation
// ************** End Constants *****************

// ************* Global Variables ***************
var accessToken = "";                               // Access token used to communicate with Google API's
var emailTransporter;                               // Transporter that is used to send emails
var accountUserProfiles = [];                       // Array of account user profiles in DoubleClick
// *********** End Global Variables *************

/**
 * Sends an email.
 * @param {String} messageRecipient - The recipient of the message
 * @param {String} messageSubject  - The subject of the message
 * @param {String} messageText - The body of the message
 */
async function sendEmail(messageRecipient, messageSubject, messageText) {
    var mailOptions = setupEmailOptions(messageRecipient, messageSubject, messageText);
    try {
        var info = await emailTransporter.sendMail(mailOptions);
        if (String(info.response).includes("OK")) {
            console.log("Email Sent Successfully");
        }
    } catch (err) {
        console.log("Email was not sent successfully.", err);
    }
}

/**
 * Sets up the email transporter object used to send emails.
 */
function setupEmailTransporter() {
    emailTransporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
          user: automatedEmailSender,
          pass: automatedEmailPass
        }
    });
}

/**
 * Sets up the email to be sent.
 * @param {String} messageRecipient - The recipient of the message
 * @param {String} messageSubject  - The subject of the message
 * @param {String} messageText - The body of the message
 */
function setupEmailOptions(messageRecipient, messageSubject, messageText) {
    return {
        from: automatedEmailSender,
        to: messageRecipient,
        subject: messageSubject,
        text: messageText
    }
}

/**
 * Gets/Updates the access token by using the configured
 * refresh token, clientID and clientSecret.
 */
async function updateAccessToken() {
    const requestOptions = getAccessTokenRequestConfig();
    try {
        const result = await axios(requestOptions);
        accessToken = result.data.access_token;
    } catch (err) {
        console.log('There was an error', err);
    }
}

/**
 * Creates the configuration of the request that will
 * be sent to Google's Auth service to update the access
 * token
 */
function getAccessTokenRequestConfig() {
    const postBody = `grant_type=refresh_token` +
        `&client_id=${encodeURIComponent(clientID)}` + 
        `&client_secret=${encodeURIComponent(clientSecret)}` + 
        `&refresh_token=${encodeURIComponent(refreshToken)}`;

    const requestOptions = {  
        url: 'https://www.googleapis.com/oauth2/v4/token',
        method: 'post',
        data: postBody,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    };
    return requestOptions;
}

/**
 * Returns the active threads that fit within the
 * specified search criteria in the configuration
 * at the top of the file.
 */
async function getActiveThreads() {
    var returnArray = [];
    const requestOptions = getActiveThreadsRequestConfig();
    try {
        const result = await axios(requestOptions);
        var returnObj = result.data;
        if (returnObj.resultSizeEstimate > 0) {
            for (threadObj of returnObj.threads) {
                returnArray.push(threadObj.id);
            }
        }
    } catch (err) {
        console.log('There was an error', err);
    }
    return returnArray;
}

/**
 * Creates the configuration of the request that will
 * retrieve thread ID's of the specified criteria above
 */
function getActiveThreadsRequestConfig() {
    const authHeaderValue = "Bearer " + accessToken;
    const requestOptions = {
        url: 'https://www.googleapis.com/gmail/v1/users/me/threads',
        params: { labelIds: threadLabels },
        paramsSerializer: function(params) {
            return qs.stringify(params, {arrayFormat: 'repeat'});
        },
        method: 'get',
        headers: {
            'Authorization': authHeaderValue
        }
    }
    return requestOptions;
}

/**
 * Sends a request to Gmail API to retrieve the complete
 * email from the given thread ID.
 * @param {String} threadID the id of the email to retrieve
 */
async function getEmailFromThreadID(threadID) {
    var returnObj = {};
    const requestOptions = getEmailFromThreadIDRequestConfig(threadID);
    try {
        const result = await axios(requestOptions);
        returnObj = result.data;
    } catch (err) {
        console.log('There was an error', err);
    }
    return returnObj;
}

/**
 * Creates a request for retrieving an email from a given
 * thread ID
 * @param {String} threadID the id of the email to create a request for
 */
function getEmailFromThreadIDRequestConfig(threadID) {
    const authHeaderValue = "Bearer " + accessToken;
    const completeURL = 'https://www.googleapis.com/gmail/v1/users/me/threads/' + threadID;
    const requestOptions = {
        url: completeURL,
        method: 'get',
        headers: {
            'Authorization': authHeaderValue
        }
    }
    return requestOptions;
}

/**
 * Returns an array of unread email objects that fit
 * the search criteria specified above.
 */
async function getUnreadEmails() {
    var returnArray = [];
    var unreadThreadIDs = await getActiveThreads();
    for (threadID of unreadThreadIDs) {
        var tempObj = await getEmailFromThreadID(threadID);
        returnArray.push(convertRawEmailToObj(tempObj));
    }
    return returnArray;
}

/**
 * Returns an object representation of the email in a usable format.
 * @param {Obj} rawEmailObj The email object to convert
 */
function convertRawEmailToObj(rawEmailObj) {
    var returnObj = {};
    returnObj.id = rawEmailObj.id;
    const messageObj = rawEmailObj.messages[(rawEmailObj.messages.length - 1)]; // Last Message in Email
    returnObj.receivedFrom = getReceivedFromInfo(messageObj.payload.headers);
    returnObj.body = convertBase64StringToAscii(messageObj.payload.parts[0].body.data);
    return returnObj;
}

/**
 * Decodes the base64 string and returns the ascii string
 * @param {String} base64String Base64 String to convert
 */
function convertBase64StringToAscii(base64String) {
    return new Buffer(base64String, 'base64').toString('ascii');
}

/**
 * Returns the "From" value for the email
 * @param {List[Obj]} emailHeaders headers on the email
 */
function getReceivedFromInfo(emailHeaders) {
    var returnString = "";
    for (tempHeader of emailHeaders) {
        if (tempHeader.name && tempHeader.name === "From") {
            returnString = tempHeader.value;
            break;
        }
    }
    return returnString;
}

/**
 * Returns an array of names extracted from the email that fit
 * the format of "<name> <date>"
 * @param {String} emailBody body of the email
 */
function getNamesFromEmailBody(emailBody) {
    var returnArray = [];
    var testArray = emailBody.split('\r\n');
    for (tempItem of testArray) {
        if (isNameDateObj(tempItem)) {
            returnArray.push(getNameFromNameDateObj(tempItem));
        }
    }
    return returnArray;
}

/**
 * Returns true if the String is in the format "<name> <date>"
 * false otherwise.
 * @param {String} inputString the string to test
 */
function isNameDateObj(inputString) {
    return emailDateRegex.test(inputString);
}

/**
 * Returns the name from the name date string
 * @param {String} nameDateObj string to extract the name from
 */
function getNameFromNameDateObj(nameDateObj) {
    return nameDateObj.split(/\d{1,2}\/\d{1,2}\/\d{4}/)[0];
}

/**
 * Gets all user profiles tied to each double click account
 */
async function getAccountUserProfiles() {
    var returnArray = [];
    for (profileID of doubleClickProfileIDs) {
        const requestOptions = getAccountUserProfileRequestConfig(profileID);
        try {
            const result = await axios(requestOptions);
            if (result.data.accountUserProfiles) {
                for (tempProfile of result.data.accountUserProfiles) {
                    tempProfile.doubleClickProfileID = profileID;
                    // if (!profileAlreadyExists(returnArray, tempProfile)) {
                    //     returnArray.push(tempProfile);
                    // }
                    returnArray.push(tempProfile);
                }
            }
        } catch (err) {
            console.log("There was an error", err);
        }
    }
    return returnArray;
}

/**
 * Gets the list of user profiles for the given profile ID
 * @param {String} profileID profile ID to get user profiles for
 */
function getAccountUserProfileRequestConfig(profileID) {
    const authHeaderValue = "Bearer " + accessToken;
    const requestURL = `https://www.googleapis.com/dfareporting/v3.2/`
    + `userprofiles/${profileID}/accountUserProfiles`;
    const requestOptions = {  
        url: requestURL,
        method: 'get',
        headers: {
            'Authorization': authHeaderValue
        }
    };
    return requestOptions;
}

/**
 * Returns the double click information for a given name
 * @param {String} name the name of the user to get DoubleClick information for
 */
function getDoubleClickProfileForName(name) {
    var returnValue = {};
    for (tempProfile of accountUserProfiles) {
        if (emailContainsName(tempProfile, name)) {
            returnValue = tempProfile;
            break;
        }
    }
    return returnValue;
}

/**
 * Returns true if the email on the user profile contains the given
 * name, false otherwise.
 * @param {Object} userProfile the double click user profile object
 * @param {String} name the name of the user
 */
function emailContainsName(userProfile, name) {
    var returnValue = false;
    var modifiedEmail = userProfile.email.toLowerCase().trim();
    var emailIndex = modifiedEmail.indexOf("@");
    if (emailIndex != -1) {
        modifiedEmail = modifiedEmail.substring(0, emailIndex);
    }
    var modifiedName = name.toLowerCase().replace(' ', '.').trim();
    if (modifiedEmail === modifiedName) {
        returnValue = true;
    }
    return returnValue;
}

/**
 * Appends DoubleClick information for each of the names found
 * under all double click profiles.
 * @param {Object} emailObj email object
 * @param {Array[String]} nameArray array of names  
 */
function addDoubleClickInfoToEmailObj(emailObj, nameArray) {
    var returnObj = emailObj;
    var doubleClickInfoArray = [];
    for (name of nameArray) {
        var userProfile = getDoubleClickProfileForName(name);
        if (userProfile != {}) {
            doubleClickInfoArray.push({
                name: name,
                doubleClickID: userProfile.id,
                doubleClickIsActive: userProfile.active,
                doubleClickParentProfileID: userProfile.doubleClickProfileID
            })
        }
    }
    returnObj.doubleClickInfoArray = doubleClickInfoArray;
    return returnObj;
}

/**
 * Removes the "Unread" tag from the specified email
 * @param {Object} emailObj Email Object to modify
 */
function markEmailAsRead(emailObj) {
    const requestOptions = getMarkEmailAsReadRequestConfig(emailObj.id);
    try {
        axios(requestOptions);
    } catch (err) {
        console.log('There was an error', err);
    }
}

/**
 * Configuration of the request for removing "unread" label from an email
 * @param {String} threadID thread ID to modify
 */
function getMarkEmailAsReadRequestConfig(threadID) {
    const requestURL = `https://www.googleapis.com/gmail/v1/users/me`
        + `/threads/${threadID}/modify`;
    const requestOptions = {  
        url: requestURL,
        method: 'post',
        headers: {
            'Authorization': ('Bearer ' + accessToken)
        },
        data: {
            "removeLabelIds": [
                "UNREAD"
            ]
        }
    };
    return requestOptions;
}

/**
 * Deactives all users from the array in double click where the user is not
 * already deactivated.
 * @param {List[Object]} doubleClickUserProfiles double click profile information
 */
function deactiveUserProfilesInDoubleClick(doubleClickUserProfiles) {
    for (doubleClickUserProfileObj of doubleClickUserProfiles) {
        if (doubleClickUserProfileObj.doubleClickIsActive === true) {
            sendDoubleClickDeactivationRequest(doubleClickUserProfileObj.doubleClickID, 
                doubleClickUserProfileObj.doubleClickParentProfileID);
            console.log(`${doubleClickUserProfileObj.name} Deactivated in DoubleClick.`);
        }
    }
}

/**
 * Sends a request to deactive a user in DoubleClick
 * @param {String} doubleClickID User ID to deactivate
 */
function sendDoubleClickDeactivationRequest(doubleClickID, doubleClickParentProfileID) {
    const requestOptions = getDoubleClickDeactivationRequestConfig(doubleClickID, doubleClickParentProfileID);
    try {
        axios(requestOptions);
    } catch (err) {
        console.log("There was an error", err);
    }
}

/**
 * Creates the configuration for the request to deactive a user
 * in DoubleClick.
 * @param {String} doubleClickID User ID to deactivate
 */
function getDoubleClickDeactivationRequestConfig(doubleClickID, doubleClickParentProfileID) {
    const authHeaderValue = "Bearer " + accessToken;
    const requestURL = `https://www.googleapis.com/dfareporting/v3.2/userprofiles/`
        + `${doubleClickParentProfileID}/accountUserProfiles?id=${doubleClickID}`;
    const requestOptions = {  
        url: requestURL,
        method: 'patch',
        headers: {
            'Authorization': authHeaderValue,
            'Content-Type' : 'application/json'
        },
        data: {
            "active" : false
        }
    };
    return requestOptions;
}

/**
 * Creates an email body to be sent to the user as a follow up
 * for deativating users in DoubleClick.
 * @param {Object} emailObj Email Object to Parse for Body
 */
function createFollowUpEmailBody(emailObj) {
    var returnString = "";
    var usersDeactivated = [];
    var usersNotFound = [];
    for (tempDoubleClickObj of emailObj.doubleClickInfoArray) {
        if (tempDoubleClickObj.doubleClickID) {
            usersDeactivated.push(tempDoubleClickObj.name);
        } else {
            usersNotFound.push(tempDoubleClickObj.name);
        }
    }
    returnString = `Message Received From: ${emailObj.receivedFrom}\n`
        + `Users Found and Deactivated: ${JSON.stringify(usersDeactivated)}\n`
        + `Users Not Found in DoubleClick: ${JSON.stringify(usersNotFound)}\n`
        + `Original Message Body: \n----------\n${emailObj.body}\n----------\n`;
    return returnString;
}

/**
 * Reaches out to Google's Gmail feed to get unread emails for a user
 * and runs automation on the emails if the email is intended to 
 * trigger automation.
 */
async function main() {
    await updateAccessToken();
    console.log('Checking for Automation Emails...');
    var unreadEmailArray = await getUnreadEmails();
    if (unreadEmailArray.length > 0) {
        accountUserProfiles = await getAccountUserProfiles();
    }
    for (emailObj of unreadEmailArray) {
        markEmailAsRead(emailObj);
        nameArray = getNamesFromEmailBody(emailObj.body);
        emailObj = addDoubleClickInfoToEmailObj(emailObj, nameArray);
        deactiveUserProfilesInDoubleClick(emailObj.doubleClickInfoArray);
        sendEmail(emailObj.receivedFrom, followUpEmailSubject.replace('@@', emailObj.receivedFrom), createFollowUpEmailBody(emailObj));
    }
}

console.log("Openning Automation Channel");
setupEmailTransporter();
setInterval(main, 10000);